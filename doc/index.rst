*Versione 1.0.0 del 8 Ottobre 2020*

EP Boilerplate
================
Questo progetto è stato creato come template di applicazione che sfrutta Angular, Ionic e l'integrazione con la libreria di EP framework. Questo codice può essere utilizzato come base di partenza per lo sviluppo di un progetto o semplicemente essere preso come esempio.

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   USING

.. toctree::
   :maxdepth: 2
   :caption: Services

   EPAuthService

   EPStoreService


.. toctree::
   :maxdepth: 2
   :caption: Components

   EPLoginComponent

.. toctree::
   :maxdepth: 2
   :caption: Ionic

   Ionic

.. toctree::
   :maxdepth: 2
   :caption: NGX-Core

   ngx-core



