@EP-FRAMEWORK/NGX-CORE
=======================

Vengono esposti i seguenti servizi:

:ref:`EPAuthService <auth_service-deps>`

:ref:`EPStoreService <store-sservice-deps>`

il modulo EPModule che contiene i seguenti componenti:

:ref:`EPLoginComponent <login-component-deps>`

L'importazione dell'EPModule va fatta nel modulo che richiama i componenti in cui vogliamo usare il componente della libreria.
Tipicamente possiamo importarlo nell'app.module.ts, o in qualsiasi altro modulo (ad esempio se usiamo un router) in questo modo:

.. code-block:: typescript

    import { EPModule } from '@ep-framework/core';
    @NgModule({
        //...
        imports: [
            //...
            EPModule,
        ],
        //...
    })
    export class AppModule {}

**Non è necessario importare il modulo se si intende usare solo i servizi.**