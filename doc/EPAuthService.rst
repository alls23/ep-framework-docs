.. _usage-docs:

EP Authentication Service
=============

Espone delle utility per la gestione degli utenti dell'applicazione, dalla registrazione al reset della password, al controllo dell'utente attualmente loggato.

.. _auth_service-deps:

Come usarlo
^^^^^^^^^^^^

importalo così:

.. code-block:: typescript

    import { EPAuthService } from '@ep-framework/core';

ed iniettalo nel costruttore, come un qualsiasi servizio:

.. code-block:: typescript

    interface MyUser extends EPUser {
        additionalProp1: string;
        additionalProp2: number;
    }
    export class exampleComponent {}
    //...
    constructor(private authService: EPAuthService<MyUser>) {}
    //
    }

Il servizio riceve in input il tipo dell'utente che deve estendere EPUser. Se non hai bisogno di dichiarare proprietà aggiuntive, puoi semplicemente passare EPUser, importandolo da @ep-framework/core.

L'interfaccia base dell'utente EPUser è la seguente:

.. code-block:: typescript

    interface EPUser {
        uid: string;
        email: string;
        emailVerified: boolean;
        roles?: string[];
    }


Current User
^^^^^^^^^^^^^

CurrentUser è un Behavior Subject che viene inizialmente valorizzato con null.
Se un utente autenticato esegue il refresh della pagina (ad esempio con f5), currentUser torna al suo valore iniziale null per poi ricevere l'utente autenticato.
Per essere sicuri di accedere al valore corretto, bisogna attendere la risoluzione del metodo isAuth(), di cui parleremo più avanti.
Non appena l'utente esegue il logout, currentUser tornerà automaticamente null.
È possibile accedere a tutte le proprietà del documento Firestore dell'utente attualmente loggato, semplicemente con authService.currentUser.value.

Metodi esposti:
^^^^^^^^^^^^^^

isAuth
~~~~~~

.. code-block:: javascript

    async isAuth(): Promise<boolean>

questo metodo può essere usato ad esempio nelle guardie per verificare se l'utente è autenticato o no.
Oppure si può aspettare che la Promise si risolva per essere certi che l'utente sia autenticato oppure no (e quindi mostrare i contenuti appropriati solo dopo la sua risoluzione così da evitare flickering)

Registrazione con email e password
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    registerWithEmailAndPassword(email: string, password: string, roles?: string[]): Promise<EPUser>;

Login con email e password
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    loginWithEmailAndPassword(email: string, password: string): Promise<EPUser>;

Login tramite google
~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    googleSignin(): Promise<EPUser>;

Logout
~~~~~~

.. code-block:: javascript

    logout(): Promise<void>;

Invio della mail con link di verifica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    sendVerificationEmail(): Promise<void>;

Invio della mail per il reset della password
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    resetPasswordInit(email: string, action?: string): Promise<void>;

il campo opzionale action può contenere l'URL verso cui reindirizzare l'utente una volta resettata la password.
