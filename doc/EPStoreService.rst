EP Store Service
==============

Espone dei metodi semplificati per leggere/inserire/modificare collezioni e documenti sul database Firestore.

.. _store-sservice-deps:

Come usarlo
^^^^^^^^^^^

importalo così:

.. code-block:: typescript

    import { EPStoreService } from '@ep-framework/core';

ed iniettalo nel costruttore, come un qualsiasi servizio:

.. code-block:: typescript

    export class exampleComponent {}
    //...
        constructor(private storeService: EPStoreService) {}
    //
    }

Metodi esposti
^^^^^^^^^^^^^^

*Tutti i metodi di accesso ai documenti iniettano l'id del documento nell'oggetto che viene restituito*

collection$
~~~~~~~~~~~
.. code-block:: typescript

    collection$<O extends EPObject>(path: string, query?: QueryFn): Observable<O[]>;
Restituisce un osservablie, collegato in tempo reale con Firestore, che emette un'array di documenti contenuti nella collezione.

Il parametro query di tipo QueryFn va espresso in questo modo:

.. code-block:: typescript

    this.storeService.collection$('/users', ref => ref.where('email', '==', 'email@test.it'));

doc$
~~~~~
.. code-block:: typescript

    doc$<O extends EPObject>(path: string): Observable <O>;

Restituisce un osservabile collegato al documento richiesto.

update
~~~~~~
.. code-block:: typescript

    update(path: string, data: Object): Promise<DocumentReference | void>;

Permette di aggiornare una collezione aggiungendo un documento, o un documento, facendo il merge delle proprietà di data con quelle del documento esistente nel DB.

deleteDoc
~~~~~~~~~

.. code-block:: typescript

    deleteDoc(path: string): Promise<void>;

Cancella un documento.

deleteField
~~~~~~~~~~~

.. code-block:: typescript

    deleteField(path: string, field: string): Promise<void>;

Cancella un campo in un documento.

liveSearch
~~~~~~~~~~~

questa funzione non è ancora completa

.. code-block:: typescript

    liveSearch(path: string, field: string, comparison: WhereFilterOp, value: Observable): Observable<T[]>;

permette di effettuare una ricerca live su un campo del documento passando un osservabile agganciato ad esempio ad un campo testo.