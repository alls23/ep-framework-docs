.. _usage-docs:

EP Login Component
=================

Questo componente mette a disposizione un'interfaccia per permettere agli utenti di autenticarsi (tramite email e password o social), di registrarsi, e di accedere alla funzionalità **password dimenticata**.

.. _login-component-deps:

Come usarlo
^^^^^^^^^^^

Questo componente è al momento esposto direttamente dall'EPModule, per comodità riportiamo anche qui come si importa il modulo:
    L'importazione dell'EPModule va fatta nel modulo che richiama i componenti in cui vogliamo usare il componente della libreria.
    Tipicamente possiamo importarlo nell'app.module.ts, o in qualsiasi altro modulo (ad esempio se usiamo un router) in questo modo:


.. code-block:: typescript

    import { EPModule } from '@ep-framework/core';
    @NgModule({
    //...
    imports: [
        //...
        EPModule,
    ],
    //...
    })
    export class AppModule {}

per usarlo basta includerlo nel template html della pagina, attraverso il seguente tag:

.. code-block:: html

    <ep-login></ep-login>

In questo modo il componente utilizzerà le opzioni di default.

come già detto, è possibile specificare le opzioni, accedere ai dati del componente e mettersi in ascolto degli eventi sollevati dallo stesso in questo modo:

.. code-block:: html

    <ep-login [options]="settings" [(data)]="loginData" (event)="epEvent($event)"></ep-login>

dichiarando settings e loginData all'interno della logica del componente che stiamo realizzando, oltre che la funzione epEvent(e):

.. code-block:: typescript

    import { EPLoginOptions } from '@ep-framework/core';
    import { EPLoginData } from '@ep-framework/core';
    //..

    export class MyComponent implements OnInit {
    //..
        settings: EPLoginOptions = {
            type: 'normal',
            google: false,
        };
    loginData: EPLoginData = {
        username: '',
        password: ''
    }
    //..
    epEvent(e) {
        console.log('EPLogin event: ', e);
    }
    //..
}

le opzioni disponibili al momento sono:


.. code-block:: typescript

    interface EPLoginOptions extends EPComponentOptions {
        type:'compact' | 'normal';
        returnUrl?: string;
        google:boolean;
        onlyRaiseEvents: boolean; //ereditata da EPComponentOptions
    };

l'opzione returnUrl indica l'url verso cui reindirizzare l'utente in caso di login avvenuto con successo.
se viene usato la modalità compact, viene renderizzato solo un tasto che apre una modale per il login, in questo caso non viene effettuato nessun redirect: viene semplicemente chiusa la modal.

l'opzione onlyRaiseEvents impostata a true mette il componente in uno stato di mero sollevatore d'eventi: non verrà eseguito né il login, né la registrazione.
