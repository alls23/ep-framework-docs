.. _usage-docs:

Creazione nuovo progetto Ionic
==============================

Per la creazione di un nuovo progetto con Ionic, avviare il seguente comando dal terminale scegliendo il template più adatto alle proprie esigenze, tra quelli messi a disposizione.

.. code-block:: bash

    ionic start nome_progetto blank

In fase di configurazione del progetto:

1. Scegliere come framework Angular.
2. Se si desidera creare un app ibrida (iOs & Android) digitare "y", altrimenti "N".

Successivamente il framework installerà tutte le dependecies utili al funzionamento dello stesso.
Avvia ionic serve per far partire il server di sviluppo e vai a http://localhost:8100/. L'app verrà auotomaticamente caricata ad ogni modifica effettuata ai file d'origine.